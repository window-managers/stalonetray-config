# stalonetray-config
Config for stalonetray. It was configured for xmobar using the xmonad window manager. 

## Setup instructions
go to the stalonetrayrc folder

```
cd stalonetrayrc
```

Copy this config to its config location

```
cp stalonetrayrc ~/.stalonetrayrc
```

## changes
<ol>
  <li> Change grow gravity to NE, grow_gravity NE </li>
  <li> Change icon gravity to NE, icon_gravity NE </li>
  <li> Change background to black, background "#000000" </li>
  <li> Enable horizontal scrollbar, scrollbars horizontal </li>
  <li> Change max geometry width to 4, max_geometry 4x1 </li>
  <li> Chnage geometry width to 4 and set to the left of the screen, geometry 4x1-0+0 </li>
</ol>

## xmonad-and-xmobar
repo: https://gitlab.com/window-managers/xmonad-and-xmobar-config

## Credit to the creators of stalonetray

repo: https://github.com/kolbusa/stalonetray

